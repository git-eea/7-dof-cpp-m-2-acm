#include "kinematic_model.h"
#include <iostream>

int main(){  
    // Compute the forward kinematics and jacobian matrix for
    // q = M_PI/3.0, M_PI_4, -M_PI/2.0, 0.0, 

    // Define DH parameters for Franka Emika Panda robot
    /*Eigen::Vector7d*/std::array<double,8> a = {0.0, -0.400, -0.042, 0.0, 0.0, 0.0, 0.0};
    /*Eigen::Vector7d*/std::array<double,8> alpha = {M_PI_2, 0.0, -M_PI_2, M_PI_2, -M_PI_2, M_PI_2, 0.0};
    /*Eigen::Vector7d*/std::array<double,8> d = {0.333, 0.0, 0.316, 0.0, 0.384, 0.0, 0.107};
    RobotModel Augustin(a, d, alpha);

    Eigen::Vector7d 
    q(M_PI/3.0, M_PI_4, 0.0, -M_PI_2, 0.0, -M_PI/3.0, M_PI_2),
    dq(0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01),
    q_star;
    Eigen::Affine3d X, dX, X_star;
    Eigen::Vector3d  dX_star;
    Eigen::Matrix67d J, J2;

    Augustin.FwdKin(X, J, q);
    std::cout << "\nX = " << X.translation()(0) << "\nY = " << X.translation()(1) << "\nZ = " << X.translation()(0) << "\nJ = " << J << std::endl;

    // For a small variation of q, compute the variation on X and check dx = J . dq
    q_star = q+dq;

    Augustin.FwdKin(X_star, J2, q_star);
    std::cout << "\nX* = " << X_star.translation()(0) << "\nY* = " << X_star.translation()(1) << "\nZ* = " << X_star.translation()(0) << "\nJ = " << J2 << std::endl;

    //dX_star = X_star.translation() - X.translation();
    //dX = J*0.01;
    //std::cout << "\n\ndX* = " << dX_star.translation()(0) << "\ndY* = " << dX_star.translation()(1) << "\ndZ* = " << dX_star.translation()(2) << std::endl;
    //std::cout << "\n\ndX = " << dX.translation()(0) << "\ndY* = " << dX.translation()(1) << "\ndZ* = " << dX.translation()(2) << std::endl;
    //std::cout << "\n\nDifferences de X : " << "\n x-x* = " << dX.translation()(0) - dX_star.translation()(0) << "\ny-y* = " << dX.translation()(1) - dX_star.translation()(1) << "\nz-z* = " << dX.translation()(2) - dX_star.translation()(2) << std::endl;
}
