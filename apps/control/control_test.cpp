#include "control.h"
#include "trajectory_generation.h"
#include "panda_visualizer.h"
#include <iostream>
#include <unistd.h>
int main(){  

  const std::array<double,8>  a     {      0,         0,         0,    0.0825,   -0.0825,         0,    0.0880,         0  };       
  const std::array<double,8>  d     { 0.3330,         0,    0.3160,         0,    0.3840,         0,         0,    0.1070  };         
  const std::array<double,8>  alpha {      0,   -1.5708,    1.5708,    1.5708,   -1.5708,    1.5708,    1.5708,         0  };           
  // initial joint position
  const double                tf    { 3.0                               };    // final time
  Eigen::Vector7d             q     {-0.479698, 0.0723513 ,-0.012116  ,-1.99778  , 1.32045  , 1.14296 ,-0.554182};
  Eigen::Matrix<double,6,7>   J     { Eigen::Matrix<double,6,7>::Zero() };    // jacobian matrix
  RobotModel                  rm    { a, d, alpha                       };    // robot model
  Eigen::Affine3d             xi    { Eigen::Affine3d::Identity()       };    // initial pose
  rm.FwdKin(xi,J,q);                                                          // compute initial kinematic model 
  rm.FwdKin(xi,J,q);                                                          // compute initial kinematic model 
  Eigen::Affine3d             xf    { xi                                };    // define final desired pose
  xf.translation()(2)         += 0.14;
  xf.linear().matrix()        = Eigen::AngleAxisd(-M_PI/4.,Eigen::Vector3d::Unit(2)).matrix() * 
                                Eigen::AngleAxisd(-M_PI/2.,Eigen::Vector3d::Unit(0)).matrix() *
                                xf.linear().matrix();
  Point2Point                 p2p   { xi, xf, tf                        };    // generate trajectory
}