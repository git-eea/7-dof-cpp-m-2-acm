#include "trajectory_generation.h"
#include <iostream>
#include <fstream>

int main(){  
    // Compute the trajectory for given initial and final positions. #DONE
    // Display some of the computed points #DONE
    // Check numerically the velocities given by the library #DONE
    // Check initial and final velocities #DONE
    
    Eigen::Affine3d x_i = Eigen::Affine3d::Identity();
    Eigen::Affine3d x_f = Eigen::Affine3d::Identity();
    x_f.translation()(0) = 2.0;
    x_f.translation()(1) = 3.0;
    x_f.translation()(2) = 5.0;
    x_f.linear()(1,0) = 1.0;
    x_f.linear()(2,1) = 1.0;
    x_f.linear()(0,2) = -1.0;
    Eigen::Affine3d X, Xt;
    double Xx, Xy, Xz; 
    
    Eigen::Vector6d V;
    Eigen::Vector3d V2;
    double tf = 3;

    Point2Point init(x_i, x_f, tf);

    std::ofstream myfile;
    myfile.open("trajectory_test.csv");
    myfile << "X,Y,Z,Vx1,Vy1,Vz1,Vx2,Vy2,Vz2,t\n";
    double dt = 0.01;
    double t = 0.0;

    while(t<tf){
        X = init.X(t);
        V = init.dX(t);
        Xt = init.X(t-dt);

        V2 = (X.translation()-Xt.translation())/(dt);

        Xx = X.translation()(0);
        Xy = X.translation()(1);
        Xz = X.translation()(2);
        myfile << Xx << "," << Xy << "," << Xz << "," << V[0] << "," << V[1] << "," << V[2] << "," << V2[0] << "," << V2[1] << "," << V[2] << "," << t << "\n";

    t+=dt;
    }

myfile.close();
}