add_definitions(-DNON_MATLAB_PARSING)
add_library( remoteApi SHARED IMPORTED )
set_target_properties( remoteApi PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/src/visualizer/remoteApi.so )
add_library(panda_visualizer STATIC ${CMAKE_SOURCE_DIR}/src/visualizer/panda_visualizer.cpp)
target_link_libraries (panda_visualizer Eigen3::Eigen remoteApi  )
target_include_directories(panda_visualizer PUBLIC 
                                                  "$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/include/visualizer>"
                                                  )