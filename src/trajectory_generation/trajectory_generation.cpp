#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //initialize the object polynomial coefficients
  double D = pfIn - piIn;
  a[0] = piIn;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10/(pow(DtIn, 3)) * D;
  a[4] = -15/(pow(DtIn, 4)) * D;
  a[5] = 6/(pow(DtIn, 5)) * D;
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //update polynomial coefficients
  double D = pfIn - piIn;
  a[0] = piIn;
  a[3] = 10 / (pow(DtIn, 3)) * D;
  a[4] = -15 / (pow(DtIn, 4)) * D;
  a[5] = 6 / (pow(DtIn, 5)) * D;
};

const double  Polynomial::p     (const double &t){
    //compute position
  double pos = a[0] + a[1]*t + a[2]*pow(t, 2) + a[3]*pow(t, 3) + a[4]*pow(t, 4) + a[5]*pow(t, 5);
  return pos;
};

const double  Polynomial::dp    (const double &t){
  //compute velocity
  double v = a[1] + 2*a[2]*t + 3*a[3]*pow(t, 2) + 4*a[4]*pow(t, 3) + 5*a[5]*pow(t, 4);
  return v;
};

Point2Point::Point2Point(const Eigen::Affine3d & X_i, const Eigen::Affine3d & X_f, const double & DtIn){
  //initialize object and polynomials
  Eigen::Vector3d Ti, Tf;
  Eigen::Matrix3d Ri, Rf;
  
  Ti = X_i.translation();
  Tf = X_f.translation();
  Ri = X_i.rotation();
  Rf = X_f.rotation();

  polx.update(Ti[0], Tf[0], DtIn);
  poly.update(Ti[1], Tf[1], DtIn);
  polz.update(Ti[2], Tf[2], DtIn);

  R0 = Rf * Ri.transpose();
  rot_aa = R0;

  axis = rot_aa.axis();
  pol_angle.update(0, rot_aa.angle(), DtIn);

  Dt = DtIn;
}

Eigen::Affine3d Point2Point::X(const double & time){
  //compute cartesian position
  Eigen::Affine3d Xpos;

  Xpos.translation()(0) = polx.p(time);
  Xpos.translation()(1) = poly.p(time);
  Xpos.translation()(2) = polz.p(time);

  Xpos.linear() = Eigen::AngleAxisd(pol_angle.p(time), axis).matrix();

  return Xpos;
}

Eigen::Vector6d Point2Point::dX(const double & time){
  //compute cartesian velocity
  Eigen::Vector6d Xvel;

  Xvel(0) = polx.dp(time);
  Xvel(1) = poly.dp(time);
  Xvel(2) = polz.dp(time);
  
  Xvel(3) = axis(0) * pol_angle.dp(time);
  Xvel(4) = axis(1) * pol_angle.dp(time);
  Xvel(5) = axis(2) * pol_angle.dp(time);

  return Xvel;
}