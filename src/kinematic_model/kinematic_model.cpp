#include "kinematic_model.h"
#include <iostream>
RobotModel::RobotModel(const std::array<double,8> &aIn, const std::array<double,8> &dIn, const std::array<double,8> &alphaIn):
  a     {aIn},
  d     {dIn},
  alpha {alphaIn}
{ 
};

void RobotModel::FwdKin(Eigen::Affine3d &xOut, Eigen::Matrix67d &JOut, const Eigen::Vector7d & qIn){
    // Initialize transformation matrices
    Eigen::Affine3d T = Eigen::Affine3d::Identity();
    Eigen::Matrix67d J = Eigen::Matrix67d::Zero();
    
    // Compute forward kinematics
    for (int i = 0; i < 7; i++) {
        Eigen::Affine3d A;
        //A = Eigen::Translation3d(Eigen::Vector3d(0.0, 0.0, d[i])) * Eigen::AngleAxisd(qIn[i] + alpha[i], Eigen::Vector3d(0.0, 0.0, 1.0)) * Eigen::Translation3d(Eigen::Vector3d(a[i], 0.0, 0.0)) * Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d(0.0, 1.0, 0.0));
        A = Eigen::AngleAxisd(qIn[i] + alpha[i], Eigen::Vector3d(0.0, 0.0, 1.0)) * Eigen::Translation3d(Eigen::Vector3d(a[i], 0.0, 0.0)) * Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d(0.0, 1.0, 0.0)) * Eigen::Translation3d(Eigen::Vector3d(0.0, 0.0, d[i]));
        T = T * A;
        
        // Compute the Jacobian
        Eigen::Vector3d z_i = T.linear().col(2); // Z-axis of the current frame
        Eigen::Vector3d p_i = T.translation();   // Origin of the current frame
        
        J.block(0, i, 3, 1) = z_i.cross(T.translation());
        J.block(3, i, 3, 1) = z_i;
    }
    
    // Set the output transformation matrix
    xOut = T;
    
    // Set the output Jacobian matrix
    JOut = J;
}